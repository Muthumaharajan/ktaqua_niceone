<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $inStock = "y"; 
    protected $outOfStock = "n"; 
    protected $fillable = [
        'name',
        'image_name',
        'description',
        'colors',
        'price',
        'discount',
        'tag',
        'category_id',
        'status'
    ];

    public function category()
    {
    	return $this->belongsTo('App\Category','category_id','id');
    }
    
    
}
