<?php

namespace App\Http\Controllers\admin_panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Category;
use App\sale;
use App\User;
use App\Address;
use App\DataTables\OrderManagementDataTable;
use App\DataTables\OrderUpdateDataTable;
use Yajra\DataTables\DataTables;

class managementController extends Controller
{
    public function manage()
    {
    	$res1= sale::orderby('created_at','desc')->limit(200)->get();
        if(!$res1)
        {
			return view('admin_panel.dashboard.orderManagement')->with('all',[])
	         ->with('products',[])
	         ->with('sale',[]);
        }
        
        $cart=[];
        $product=[];
        $users=[];
       
        foreach($res1 as $r )
        {
            
            $user = DB::select( DB::raw("select users.id as id ,users.email as email, users.phone as phone , users.full_name as full_name , addresses.area as area , addresses.city as city , addresses.zip as zip from users inner join addresses on users.address_id = addresses.id where users.id = $r->user_id " ) );
            $users[]=$user[0];
           
             $totalCart = explode(',',$r->product_id);
             foreach($totalCart as $c)
             {
                $cart[]=array_prepend(explode(':',$c), isset($r->id) ? $r->id : 1);
                $a=explode(':',$c);
                $res = Product::find($a[0]);
                $product[]=$res;
             }
        }
        
        //   dd($users);
        // dd($users[0]->area);
         return view('admin_panel.orders.index1')->with('all',$cart)
         ->with('products',$product)
         ->with('sale',$res1)
         ->with('users',$users)
         ->with('status',['Placed','On Process','Delivered','Cancel']);
        // return $dataTable->render('admin_panel.orders.index');  
    }

    public function ajaxOrderManage(){
        $sales =  sale::all();
        return Datatables::of($sales)->make(true);
    }

    public function update(Request $r)
    {
        $details = [];
        $img = explode('|', $r->img);
 
        for ($i = 0; $i < count($img) - 1; $i++) {

        if (strpos($img[$i], 'data:image/jpeg;base64,') === 0) {
            $img[$i] = str_replace('data:image/jpeg;base64,', '', $img[$i]);  
            $ext = '.jpg';
        }
        if (strpos($img[$i], 'data:image/png;base64,') === 0) { 
            $img[$i] = str_replace('data:image/png;base64,', '', $img[$i]); 
            $ext = '.png';
        }

    	if(isset($r->orderId))
    	{
    		$n['order_status'] = $r->stat;
            $n['delivery_receipt_name'] = "1".$ext;

    		$n = sale::updateOrCreate(['id'=>$r->orderId],$n);
            $totalCart = explode(',',$n->product_id);
            foreach($totalCart as $c)
            {
               $cart[]=array_prepend(explode(':',$c), isset($n->id) ? $n->id : 1);
            }
            $user = \App\User::find($n->user_id);
            $details = [
                'title' => 'Mail from K.T Aquarium Order status',
                'body' => 'Hi '.$user->full_name.'! Your Order '.$n->order_status,
                'sales' => $n,
                'products' => Product::all(),
                'all' => $cart,
                'user' => $n->users,
            ];
           
    	}
        

        $img[$i] = str_replace(' ', '+', $img[$i]);
        $data = base64_decode($img[$i]);
        
        $temp_string='/uploads/delivery_receipt/'.$n->id;
        $temp_string2='/uploads/delivery_receipt/'.$n->id;

        if (!file_exists(config('app.new_public_path').$temp_string)) {
            mkdir( config('app.new_public_path').$temp_string, 0777, true);
            
            $file = config('app.new_public_path').$temp_string2.'/1'.$ext;
            
        if (file_put_contents($file, $data)) {
            echo "<p>Image $i was saved as $file.</p>";
        } else {
            echo '<p>Image $i could not be saved.</p>';
        } 
        }
    }
    \Mail::to($user->email)->send(new \App\Mail\OrderStatusEmail($details));
    
         return redirect()->route('admin.listOrder');

    }
    public function list(OrderUpdateDataTable $dataTable){
        // $sales = sale::select()->with('users')->get();
        // return view('admin_panel.orders.update')
        // ->with('sales',$sales)
        // ->with('status',['Placed','On Process','Delivered','Cancel']);
        return $dataTable->render('admin_panel.orders.updatemain');  

    }
    public function ajaxOrderList(){
        $sales =  sale::all();
        return Datatables::of($sales)->make(true);
    }
}
