<?php

namespace App\Http\Controllers\admin_panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\sale;
use Yajra\DataTables\DataTables;
use App\DataTables\SalesDataTable;  

class dashboardController extends Controller
{
    public function index(SalesDataTable $dataTable){
        // $sales =  sale::all();
        // return view('admin_panel.dashboard.index')
        // ->with('sales',$sales);
        return $dataTable->render('admin_panel.dashboard.index');  
    }
    public function ajaxSales(){
        $sales =  sale::all();
        return Datatables::of($sales)->make(true);
    }
}
