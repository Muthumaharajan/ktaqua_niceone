<?php  
    
namespace App\DataTables;  
    
use App\User;  
use Yajra\DataTables\Html\Button;  
use Yajra\DataTables\Html\Column;  
use Yajra\DataTables\Html\Editor\Editor;  
use Yajra\DataTables\Html\Editor\Fields;  
use Yajra\DataTables\Services\DataTable;
use App\sale;

    
class SalesDataTable extends DataTable  
{  
    /**  
     * It is used to Build a class of DataTable.  
     *  
     * @param mixed $query Results from query() method.  
     * @return \Yajra\DataTables\DataTableAbstract  
     */  
    public function dataTable($query)  
    {  
        return datatables()  
            ->eloquent($query)
           ->addColumn('Customer_Receipt', function ($row) {
                return '<a download="order-'.$row->id.'-customer_receipt.jpg" href="'.asset('/public/uploads/customer_receipt/'.$row->id.'/'.$row->customer_receipt_name).'" title="ImageName">
                <img src="'.asset('/public/uploads/customer_receipt/'.$row->id.'/'.$row->customer_receipt_name).'" onerror="this.src='.asset('public/img/no-image.png').'" alt="">
                </a>';
            })->editColumn('created_at',function($object){
                    return  $object->created_at->diffForHumans();   
                })->addColumn('', function ($row) {
                return '<div class="form-check">
                        <input class="form-check-input check-selection" type="checkbox" value="" id="check-question-'.$row->id.'">
                    </div>';
            })->rawColumns(['Customer_Receipt','']);
    }  
    
    /**  
     * It is used to get query sources of dataTable.  
     *  
     * @param \App\User $model  
     * @return \Illuminate\Database\Eloquent\Builder  
     */  
    public function query(SalesDataTable $model)
    {
        //return $model->newQuery();
        $data = sale::with('users')->select('sales.*');
        return $data;
    }
    
    /**  
     * If you want to use html builder, it is the optional method.  
     *  
     * @return \Yajra\DataTables\Html\Builder  
     */  
    public function html()  
    {  
        return $this->builder()  
                    ->columns($this->getColumns())  
                    ->minifiedAjax()  
                    ->orderBy(1)  
                    ->parameters([  
                        'dom'          => 'Bfrtip',  
                        'buttons'      => ['excel', 'csv'],  
                    ]);  
    }  
    
    /**  
     * It is used to get columns.  
     *  
     * @return array  
     */  
    protected function getColumns()  
    {  
        return ['', 
            'id', 
            'user_name'=> new \Yajra\DataTables\Html\Column([
                'title' => 'User Name', 
                'data' => 'users.full_name',
                'name' => 'users.full_name'
               ]),
            'user_email'=> new \Yajra\DataTables\Html\Column([
                'title' => 'User Name', 
                'data' => 'users.email',
                'name' => 'users.email'
                ]),
            'user_phone'=> new \Yajra\DataTables\Html\Column([
                'title' => 'User Name', 
                'data' => 'users.phone',
                'name' => 'users.phone'
                ]),
            'created_at',  
            'order_status',  
            'price',
            'Customer_Receipt'
        ];  
    }  
    
    /**  
     * It is used to get the filename for export.  
     *  
     * @return string  
     */  
    protected function filename()  
    {  
        return 'Sales_' . date('YmdHis');  
    }  
}  