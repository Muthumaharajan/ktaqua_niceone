<?php

namespace App\DataTables;

use App\App\OrderUpdate;
use App\sale;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;

class OrderUpdateDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query) 
            ->addColumn('DReceipt', function ($row) {
                return '<a download="order-'.$row->id.'-delivery_receipt.jpg" href="'.asset('/public/uploads/delivery_receipt/'.$row->id.'/'.$row->delivery_receipt_name).'" title="ImageName">
                <img src="'.asset('/public/uploads/delivery_receipt/'.$row->id.'/'.$row->delivery_receipt_name).'" onerror="this.src='.asset('public/img/no-image.png').'" alt="">
                </a>';
            })->addColumn('CReceipt', function ($row) {
                return '<a download="order-'.$row->id.'-customer_receipt.jpg" href="'.asset('/public/uploads/customer_receipt/'.$row->id.'/'.$row->customer_receipt_name).'" title="ImageName">
                <img src="'.asset('/public/uploads/customer_receipt/'.$row->id.'/'.$row->customer_receipt_name).'" onerror="this.src='.asset('public/img/no-image.png').'" alt="">
                </a>';
            })->addColumn('SNo', function ($row) {
                return '<input type="radio" name="sale_id" id="sale_id" class="update" value="'.$row->id.'"/>';
            }) ->editColumn('created_at',function($object){
                    return  $object->created_at->diffForHumans();   
                })->rawColumns(['SNo','CReceipt','DReceipt']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\App\OrderUpdate $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(OrderUpdateDataTable $model)
    {
        // return $model->newQuery();
        $data = sale::select()->with('users');
        return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())                   
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('SNo')
                  ->exportable(false)
                  ->printable(false)
                  ->width(20)
                  ->addClass('text-center'),
            'id'=> new \Yajra\DataTables\Html\Column([
                'title' => 'Order Id', 
                'data' => 'id',
                'name' => 'id'
               ]), 
            'user_name'=> new \Yajra\DataTables\Html\Column([
                'title' => 'User Name', 
                'data' => 'users.full_name',
                'name' => 'users.full_name'
               ]),
            'created_at'=> new \Yajra\DataTables\Html\Column([
                'title' => 'Order Created At', 
                'data' => 'created_at',
                'name' => 'created_at'
                ]),
                Column::computed('DReceipt')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
                
                Column::computed('CReceipt')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'OrderUpdate_' . date('YmdHis');
    }
}
