<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sale extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'product_id',
        'order_status',
        'price',
        'delivery_receipt_name',
        'customer_receipt_name'
    ];

    public function users()
    {
    	return $this->belongsTo(User::class, 'user_id'); 
    }

    public function products()
    {
    	return $this->belongsTo(Product::class, 'user_id'); 
    }
}
