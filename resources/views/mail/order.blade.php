<!DOCTYPE html>
<html>
<head>
    <title>K.T Aquarium Order Status Mail</title>
</head>
<body>
    <h1>{{ $orders['title'] }}</h1>
    <p>{{ $orders['body'] }}</p>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>
                        Id
                    </th>
                    <!-- <th>
                        Order Deleivery Receipt
                    </th> -->
                    <th>
                        Name
                    </th>
                    <!-- <th>
                        Adress
                    </th> -->
                    <th>
                        Product Name
                    </th>
                    <th>
                        Quantity
                    </th>
                    <th>
                        Placed at
                    </th>
                    <th>
                        Status
                    </th>

                   
                    <!-- <th>
                        Update
                    </th> -->
                </tr>
            </thead>
            <tbody>
               <?php  $s = $orders['sales']; ?>
                @foreach($orders['all'] as $c)

                @if($c[0]==$s->id)
                @foreach($orders['products'] as $p)
                @if(session()->get('user')->id == $s->user_id)

                    @if($c[1]==$p->id)
                <tr>
                <td>{{$s->id}}</td>                                
                <?php $u = session()->get('user') ?>
                            @if(isset($u->id) ? $u->id : 1 == $s->user_id)
                            <td>{{$u->full_name}}</td>
                            
                            @endif

                    <td>
                        
                        {{$p->name}}
                        
                    </td>
                    <td>
                        {{$c[2]}}
                    </td>
                    
                    <td>
                        {{$s->created_at->diffForHumans()}}
                    </td>
                    <td>
                    {{$s->order_status}}
                    </td>
                

            </tr>
            @break
            @endif
            @endif
        @endforeach
    @endif
    @endforeach
            </tbody>
        </table>
    </div>
    <img src="{{ $message->embed(config('app.new_public_path').'/uploads/customer_receipt/'.$s->id.'/'.$s->customer_receipt_name) }}" alt="No Receipt uploaded"  height="50px" width="50px">
    <img src="{{ $message->embed(config('app.new_public_path').'/img/logo.png') }}" style="max-width: 100px"/>
  
             
    <p>Thanks for Ordering</p>
</body>
</html>