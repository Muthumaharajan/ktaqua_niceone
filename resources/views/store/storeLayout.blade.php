<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>K.T Aquarium</title>

    <link rel="shortcut icon" href="{{asset('public/fav.png')}}" />
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="{{asset('public/css/bootstrap.min.css')}}" />

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="{{asset('public/css/slick.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('public/css/slick-theme.css')}}" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="{{asset('public/css/nouislider.min.css')}}" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{asset('public/css/font-awesome.min.css')}}">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="{{asset('public/css/style2.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('public/css/custom.css')}}" />

    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <!-- Include whatever JQuery which you are using -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <!-- Other js and css scripts -->
    <style>
        body{
            background:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)),url("{{asset('public/img/water.jpg')}}");
        }
        *{
            overflow-x: clip !important;
        }

        .letter-background{
            background:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)),url('{{asset("public/img/christmas_gift.jpg")}}');
        }
    </style>
</head>

<body>
<div id="snow">
    <!-- HEADER -->
    <header>
        <!-- TOP HEADER -->
        <div id="top-header">
            <div class="container">
                <ul id="head_links" class="header-links pull-left">
                    <li><a href="{{route('user.home')}}" class="logo">
                                <img src="{{asset('public/img/rm_bg.png')}}" alt="" style="width: 80px;">
                            </a></li>
                </ul>
                <ul class="header-links pull-right header-ctn">                    
                    @if(session()->has('user'))
                      <li><a style="color:white" href="{{route('user.history')}}"><i class="fa fa-user-o"></i> {{session()->get('user')->full_name}} </a></li>  
                      <li><a href="{{route('user.logout')}}"><i class="fa fa-sign-out"></i> Logout</a></li>
                    @else
                    <li><a href="{{route('user.login')}}"><i class="fa fa-sign-in"></i> Login</a></li>
                    
                    <li><a href="{{route('user.signup')}}"><i class="fa fa-registered"></i> SignUp</a></li>
                    @endif
                    <li>
                        <!-- Cart -->
                        <div  class="dropdown">
                            <a class="dropdown-toggle " id="custom_shopping_cart" href="{{route('user.cart')}}">
                                <i class="fa fa-shopping-cart"></i>
                                <span>Cart</span>
                                <label id="lblCartCount" class="badge badge-warning" style="background: #ff0000;">{{session('orderCounter')}}</label>
                            </a>

                        </div>
                        <!-- /Cart -->
                    </li>
                    <li>
                        <!-- Menu Toogle -->
                        <div class="menu-toggle pull-right">
                            <a href="#">
                                <i class="fa fa-bars"></i>
                                <span>Menu</span>
                            </a>
                        </div>
                        <!-- /Menu Toogle -->
                    </li>
                </ul>
            </div>
        </div>
        <!-- /TOP HEADER -->
    <!-- NAVIGATION -->
    <nav id="navigation">
        <!-- container -->
        <div class="container">
            <!-- responsive-nav -->
            <div id="responsive-nav">
                <!-- NAV -->
                <ul class="main-nav nav navbar-nav">
                    <li><div class="header-search">
                            <form action="{{route('user.search')}}" method="get">
                                <div class="custom_search_top" >
                                    <input class="input" style="border-radius: 40px 0px 0px 40px;" name="n" placeholder="Search here">
                                    <button  class="search-btn">Search</button>
                                </div>
                            </form>
                        </div>
                    </li>
                    <li class="{{Route::is('user.home') ? 'active' : ''}}"><a href="{{route('user.home')}}">Home</a></li>
                    @if(Route::is('user.search'))
                        @foreach($cat as $c)
                        <li class="{{$c->id == $a ? 'active' : ''}}"><a href="{{route('user.search.cat',['id'=>$c->id])}}" >{{$c->name}}</a></li>
                        @endforeach
                        <li class="{{$a == -1  ? 'active' : ''}}"><a href="search">Browse All</a></li>
                    @else
                        @foreach($cat as $c)
                        <li ><a href="{{route('user.search.cat',['id'=>$c->id])}}" >{{$c->name}}</a></li>
                        @endforeach
                        <li ><a href="{{route('user.search')}}">Browse All</a></li>
                    @endif
                    <!--<li class="{{Route::is('user.contactus') ? 'active' : ''}}"><a href="{{route('user.contactus')}}">Contact Us</a></li>-->
                </ul>
                <!-- /NAV -->
            </div>
            <!-- /responsive-nav -->
        </div>
        <!-- /container -->
    </nav>
    <!-- /NAVIGATION -->
    @include('flash-message')
    @if(Route::is('user.home'))
    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <!-- scroll start -->
                <center>
                <div class="container-fluid pb-video-container">
                    <div class="col-md-10 col-md-offset-1">
                        <h1 style="color:white;"><a href="https://www.youtube.com/channel/UCofgooCg4D-vz0GD9jozFfw" ><i class="fa fa-youtube-play text-danger" aria-hidden="true"></i></a> Welcome To K.T Aquarium Family <a href="https://www.youtube.com/channel/UCofgooCg4D-vz0GD9jozFfw" ><i class="fa fa-youtube-play text-danger" aria-hidden="true"></i></a></h1>
                        <div class="row pb-row">
                            <div class="col-md-3 pb-video">
                                <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/8pIgxnsKReo" frameborder="0" allowfullscreen></iframe>
                                <label class="form-control label-warning text-center">Treat the customer like you </label>
                            </div>
                            <div class="col-md-3 pb-video">
                                <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/cKxKajaBYeY" frameborder="0" allowfullscreen></iframe>
                                <label class="form-control label-warning text-center">Customer service = Company.</label>
                            </div>
                            <div class="col-md-3 pb-video">
                                <iframe class="pb-video-frame " width="100%" height="230" src="https://www.youtube.com/embed/_XVAqfnzp5c" frameborder="0" allowfullscreen></iframe>
                                <label class="form-control label-warning text-center">Your most happy customers</label>
                            </div>
                            <div class="col-md-3 pb-video">
                                <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/tizh9HC_nVM" frameborder="0" allowfullscreen></iframe>
                                <label class="form-control label-warning text-center">Make a customer, not a sale</label>
                            </div>
                        </div>
                    </div>
                </div>
                </center>
            <!-- scroll end -->
            <div class="row">
                <!-- shop -->
                @php
                $counter=0;
                @endphp
                @foreach($cat as $c)
                 @php
                $counter++;
                if($counter==4)
                break;
               
                @endphp
                <div class="col-md-4 col-xs-6">
                    <div class="shop">
                        <a href="search?c={{$c->id}}" class="cta-btn">
                        <div class="shop-img">
                            <img src="{{asset('public/img/shop0'.$index++.'.png')}}" alt="">
                        </div>
                        </a>
                        <div class="shop-body">
                            <a href="search?c={{$c->id}}" class="cta-btn"><h3>{{$c->name}}</h3></a>
                            <a href="search?c={{$c->id}}" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- /shop -->
                @endforeach
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- SECTION -->
    @endif


    @yield('content')

    <!-- /SECTION -->
    
    <div id="newsletter" class="section letter-background" >
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="newsletter">
                        <p style="color:white">Sign Up for the <strong>NEWSLETTER</strong></p>
                        <form>
                            <input class="input" type="email" placeholder="Enter Your Email">
                            <button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
                        </form>
                        <ul class="newsletter-follow">
                            <li>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /NEWSLETTER -->

    <!-- FOOTER -->
    <footer id="footer" >
        <!-- top footer -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row" >
                    <div class="col-md-3 col-xs-6" >
                        <div class="footer" >
                            <h3 class="footer-title">Contact Us</h3>
                            <p>Shipping available across INDIA, w.r.t Train and Bus/cargo facilities ( Metro cities Home delivery ).</p>
                            <ul class="footer-links">
                                <li><a href="tel:+91 9361765096"><i class="fa fa-phone"></i> Call</a></li>
                                <li><a class="mailto" href="mailto:ktaquarium731@gmail.com"><i class="fa fa-envelope-o"></i> Mail</a></li>
                                <li><a href="https://goo.gl/maps/MkDPbiJwXYAVhZTn6"><i class="fa fa-map-marker"></i>Location</a></li>  
                                <li><a style="color:white" href="{{route('user.history')}}">My Account</a></li>
                                <li><a class="dropdown-toggle " id="custom_shopping_cart" href="{{route('user.cart')}}">View Cart</a></li>
                               
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title">Categories</h3>
                            <ul class="footer-links">
                                @if(Route::is('user.search'))
                                    @foreach($cat as $c)
                                    <li class="{{$c->id == $a ? 'active' : ''}}"><a href="{{route('user.search.cat',['id'=>$c->id])}}" >{{$c->name}}</a></li>
                                    @endforeach
                                    <li class="{{$a == -1  ? 'active' : ''}}"><a href="search">Browse All</a></li>
                                @else
                                    @foreach($cat as $c)
                                    <li ><a href="{{route('user.search.cat',['id'=>$c->id])}}" >{{$c->name}}</a></li>
                                    @endforeach
                                    <li ><a href="{{route('user.search')}}">Browse All</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>

                    <div class="clearfix visible-xs"></div>

                    <div class="col-md-3 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title">Google Map</h3>
                            <div id="map-container-google-3" class="z-depth-1-half map-container-3">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15775.382433998246!2d77.7207516!3d8.7062078!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3fff98daa9c5e795!2sK.T%20Aquarium%20shop!5e0!3m2!1sen!2sin!4v1638893627355!5m2!1sen!2sin" width="600" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title">Policy</h3>
                            <ul class="footer-links">
                                <li><a style="color:white" href="{{route('user.privacypolicy')}}">Privacy Policy</a></li>
                                <li><a class="dropdown-toggle " id="custom_shopping_cart" href="{{route('user.refundandcancel')}}">Refund and Cancellation</a></li>
                                <li><a href="{{route('user.shippingdelivery')}}">Shipping and Delivery policy</a></li>
                                <li><a href="{{route('user.termsandcondition')}}">Terms and Condition</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /top footer -->

        <!-- bottom footer -->
        <div id="bottom-footer" class="section">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="footer-payments">
                            <li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
                            <li><a href="#"><i class="fa fa-credit-card"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
                        </ul>
                    </div>
                    <div class="footer-copyright text-center py-3"> All rights reserved. Copyright © 
                        <a href="http://ktsoftsolutions.ktaquarium.in" class="text-danger"> KT soft solutions </a>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /bottom footer -->
    </footer>
    <!-- /FOOTER -->

                                </div>
    <!-- jQuery Plugins -->
    <script src="{{asset('public/js/jquery.min.js')}}"></script>
    <script src="{{asset('public/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/js/slick.min.js')}}"></script>
    <script src="{{asset('public/js/nouislider.min.js')}}"></script>
    <script src="{{asset('public/js/jquery.zoom.min.js')}}"></script>
    <script src="{{asset('public/js/main.js')}}"></script>
    <script src="{{asset('public/js/snow1.js')}}"></script>
    <script src="{{asset('public/js/lib/jquery.js')}}"></script>
    <script src="{{asset('public/js/dist/jquery.validate.js')}}"></script>


</body>

</html>
