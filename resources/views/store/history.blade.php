@extends('store.storeLayout')
@section('content')
<?php $address = \App\Address::find(session()->get('user')->address_id);?>
<!-- SECTION -->
<div class="section bg-white">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <div class="col-md-12">
                <label class="text-success">Hi! {{session()->get('user')->full_name}}</label>
                <br>
                <div class="card card-outline">
                    <div class="card-body">
                        <p class="card-title"> Your current delivery address </p>
                        <h6 class="card-subtitle mb-2 text-muted">{{ $address->area}}</h6>
                        <small class="card-text mb-2 text-muted">{{ $address->city}} - {{ $address->zip}}</small>
                    </div>
                </div>
                <br>
                <h4 class="text-danger">Your confirmed order List</h4>
                <div class="table-responsive" >
                <table class="table">
                    <thead>
                        <th>Order Id</th>
                        <th>Image </th>
                        <th>Name</th>
                        <th>Quanity</th>
                        <th>Status</th>
                        <th>Deleivery Receipt</th>
                        <th>Your Receipt</th>
                    </thead>
                    <tbody>
                        @foreach($sale as $s)
                            @foreach($all as $c)
                            @if($c[0]==$s->id)
                            @foreach($products as $p)
                            @if(session('user')->id == $s->user_id)
                                @if($c[1]==$p->id)
                                <tr>
                                <td>{{$s->id}}</td>
                                <td><img src="../public/uploads/products/{{$p->id}}/{{$p->image_name}}" height="50px" width="50px"></td>
                                <td>{{$p->name}}</td>
                                <td>{{$c[2]}}</td>      
                                <td>{{$s->order_status}}</td>
                                <td><a download="order-{{$s->id}}-delivery_receipt.jpg" href="../public/uploads/delivery_receipt/{{$s->id}}/{{$s->delivery_receipt_name}}" title="Click to download image">
                                <img src="../public/uploads/delivery_receipt/{{$s->id}}/{{$s->delivery_receipt_name}}" onerror="this.src='{{asset('public/img/no-image.png')}}'" alt=""  height="50px" width="50px">
                                </a></td>
                                <td>
                                <a download="order-{{$s->id}}-customer_receipt.jpg" href="../public/uploads/customer_receipt/{{$s->id}}/{{$s->customer_receipt_name}}" title="Click to download image">
                                <img src="../public/uploads/customer_receipt/{{$s->id}}/{{$s->customer_receipt_name}}" onerror="this.src='{{asset('public/img/no-image.png')}}'" alt=""  height="50px" width="50px">
                                </a>
                                </td>
                                </tr>
                           
                                @break
                                @endif
                                @endif
                            @endforeach
                        @endif
                        @endforeach
                        @endforeach
                        </tbody>
                </table>
                </div>
            </div>
        </div>
        <!-- /Billing Details -->
    </div>

</div>

@endsection
