<br>
<center >
@if(session()->has('message'))
            <div class="alert {{session('alert') ?? 'alert-info'}}" style="width: 50%;">
                {{ session('message') }}
                <small class="text-danger">Please check your email</small>
            </div>
 @endif
</center>