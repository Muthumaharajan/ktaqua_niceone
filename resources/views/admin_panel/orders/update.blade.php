
@extends('admin_panel.adminLayout') @section('content')
<div class="row">
    <div class="col-md-6">
        <h4>Please select any order listed below before upload</h4>
    <table class="table table-striped">
        <thead>
            <th>SNo</th>
            <th>Order Id</th>
            <th>User Name</th>
            <th>Order Created At</th>
            <th>Receipt</th>
        </thead>
        <tbody>
            @foreach($sales as $s)
            <tr>
                <td><input type="radio" name="sale_id" id="sale_id" class="update" value="{{$s->id}}"/></td>
                <td>{{$s->id}}</td>
                <td>{{$s->users?$s->users->full_name:""}}</td>
                <td> {{$s->created_at->diffForHumans()}}</td>
                <td>
<a download="order-{{$s->id}}-delivery_receipt.jpg" href="../public/uploads/delivery_receipt/{{$s->id}}/{{$s->delivery_receipt_name}}" title="ImageName">

<img src="../public/uploads/delivery_receipt/{{$s->id}}/{{$s->delivery_receipt_name}}" onerror="this.src='{{asset('public/img/no-image.png')}}'" alt="">
</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    <div class="col-md-4" style="margin-left:172px;">
        <h4>Update order status</h4><br>
        <img  id="imageHolder" src="{{asset('public/img/no-image.png')}}" alt="add image" height="300" width="300"/>
        <br>
        <input  type="file" name="inp_files" id="inp_files" class="update" multiple="multiple" >
        <br>
        <div id="empty_image"> </div>
        <form method="post" >
            {{csrf_field()}}
            <input id="inp_img" name="img" type="hidden" value="">
            <br><br>
            <div id="for_extension_error"></div>
            <input type="hidden" name="orderId" id="orderId">
            <select name="stat" id="status" class="update" >
                <option>Select Status</option>
                @foreach($status as $x)
                @if($s->order_status!=$x)
                <option value="{{$x}}">{{$x}}</option>

                @endif

                @endforeach
            </select>
            <input type="submit" class="btn btn-sm btn-warning" value="Save" disabled>
        </form>
    </div>
</div>
<script>
     $(document).ready(
    function(){
        $('.update').change(
            function(){
                if ($('#inp_files').val()) {
                    if( $('#status').val()!="Select Status"){
                        if( $('#sale_id:checked').val()){
                            $('#orderId').val($('#sale_id:checked').val());
                            $('input:submit').attr('disabled',false);
                            // or, as has been pointed out elsewhere:
                            // $('input:submit').removeAttr('disabled'); 

                        }
                    }
                } 
            }
            );
    });
 
  function fileChange(e) {
 
     document.getElementById('inp_img').value = '';
 
     for (var i = 0; i < e.target.files.length; i++) { 
     
        var file = e.target.files[i];
 
        if (file.type == "image/jpeg" || file.type == "image/png") {
 
           var reader = new FileReader();  
           reader.onload = function(readerEvent) {
 
              var image = new Image();
              image.onload = function(imageEvent) { 
 
                 var max_size = 600;
                 var w = image.width;
                 var h = image.height;
                   
                 if (w > h) {  if (w > max_size) { h*=max_size/w; w=max_size; }
                 } else     {  if (h > max_size) { w*=max_size/h; h=max_size; } }
               
                 var canvas = document.createElement('canvas');
                 canvas.width = w;
                 canvas.height = h;
                 canvas.getContext('2d').drawImage(image, 0, 0, w, h);
                 if (file.type == "image/jpeg") {
                    var dataURL = canvas.toDataURL("image/jpeg", 1.0);
                 } else {
                    var dataURL = canvas.toDataURL("image/png");    
                 }
                 document.getElementById('inp_img').value += dataURL + '|';
              }
              image.src = readerEvent.target.result;
           }
           reader.readAsDataURL(file);
           
            readURL(this);

        } else {
           document.getElementById('inp_files').value = ''; 
           alert('Please only select images in JPG or PNG format.');   
           return false;
        }
     }
 
  }
 
  document.getElementById('inp_files').addEventListener('change', fileChange, false); 
  
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imageHolder').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    </script>
@endsection
