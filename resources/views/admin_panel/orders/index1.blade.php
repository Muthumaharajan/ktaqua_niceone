
@extends('admin_panel.adminLayout') 
@section('content')

<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Order Table</h4>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Id
                                    </th>
                                    <!-- <th>
                                        Order Deleivery Receipt
                                    </th> -->
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Contact Details
                                    </th>
                                    <th>
                                        Adress
                                    </th>
                                    <th>
                                        Product Name
                                    </th>
                                    <th>
                                        Quantity
                                    </th>
                                    <th>
                                        Placed at
                                    </th>
                                    <th>
                                        Status
                                    </th>

                                    <th>Order Created At</th>
            <th>Receipt</th>
                                    <!-- <th>
                                        Update
                                    </th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sale as $s)
                                
                                @foreach($all as $c)
                                        @if($c[0]==$s->id)
                                        @foreach($products as $p)
                                        @if($p)
                                        @if( $c[1]==$p->id)
                                <tr>
                                <td>{{$s->id}}</td>                                
                                        @foreach($users as $u)
                                            @if($u->id == $s->user_id)
                                            <td>{{$u->full_name}}</td>
                                            <td>{{$u->email}} , {{$u->phone}}</td>
                                            <td>{{$u->area}}, {{$u->city}}, {{$u->zip}} ,India</td>
                                            
                                            @break
                                            @endif
                                        @endforeach

                                    <td>
                                       
                                        {{$p->name}}
                                       
                                    </td>
                                   <td>
                                        {{$c[2]}}
                                    </td>
                                    
                                    <td>
                                        {{$s->created_at->diffForHumans()}}
                                    </td>
                                    <td>
                                    {{$s->order_status}}
                                    </td>
                                     <!--<td>
                                        <form method="post" style="display:inline-block">
                                            {{csrf_field()}}
                                            <input type="hidden" value="{{$s->id}}" name="orderId">
                                            <select name="stat">
                                                @foreach($status as $x)
                                                @if($s->order_status!=$x)
                                                <option value="{{$x}}">{{$x}}</option>

                                                @endif

                                                @endforeach
                                            </select>
                                            <input type="submit" class="btn btn-sm btn-warning" value="Update">
                                        </form>
                                    </td> -->
                                    <td><a download="order-{{$s->id}}-delivery_receipt.jpg" href="../public/uploads/delivery_receipt/{{$s->id}}/{{$s->delivery_receipt_name}}" title="ImageName">
                                <img src="../public/uploads/delivery_receipt/{{$s->id}}/{{$s->delivery_receipt_name}}" onerror="this.src='{{asset('public/img/no-image.png')}}'" alt=""  height="50px" width="50px">
                                </a></td>
                                <td>
                                <a download="order-{{$s->id}}-customer_receipt.jpg" href="../public/uploads/customer_receipt/{{$s->id}}/{{$s->customer_receipt_name}}" title="ImageName">
                                <img src="../public/uploads/customer_receipt/{{$s->id}}/{{$s->customer_receipt_name}}" onerror="this.src='{{asset('public/img/no-image.png')}}'" alt=""  height="50px" width="50px">
                                </a>
                                </td>
                                    @break
                                    @endif

                                    @endif
                                    @endforeach
                                    @endif
                                    @endforeach
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
