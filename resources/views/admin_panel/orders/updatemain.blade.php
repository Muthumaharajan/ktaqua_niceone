
@extends('admin_panel.adminLayout') 
@section('content')
<div class="row">
    <div class="col-md-5">
        <h4>Please select any order listed below before upload</h4>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> 
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> 
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script> 
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> 
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css"> 
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script> 
    <script src="{{ asset('public/vendor/datatables/buttons.server-side.js') }}"></script>   
           
                        <h4 class="card-title">Order Update</h4>
                        <div class="table-responsive-update">
                        
                        {!! $dataTable->table() !!}  

                        </div>
                   
        {!! $dataTable->scripts() !!}  
    
    </div>
    <div class="col-md-4" style="margin-left:120px;">
        <h4>Update order status</h4><br>
        <img  id="imageHolder" src="{{asset('public/img/no-image.png')}}" alt="add image" height="300" width="300"/>
        <br>
        <input  type="file" name="inp_files" id="inp_files" class="update" multiple="multiple" >
        <br>
        <div id="empty_image"> </div>
        <form method="post" >
            {{csrf_field()}}
            <input id="inp_img" name="img" type="hidden" value="">
            <br><br>
            <div id="for_extension_error"></div>
            <input type="hidden" name="orderId" id="orderId">
            <select name="stat" id="status" class="update" >
                <option>Select Status</option>
                <?php $status = ['Placed','On Process','Delivered','Cancel']; ?>
                @foreach($status as $x)
               
                <option value="{{$x}}">{{$x}}</option>

               

                @endforeach
            </select>
            <input type="submit" class="btn btn-sm btn-warning" value="Save" disabled>
        </form>
    </div>
</div>
<script>
     $(document).ready(
    function(){
        $('.update').change(
            function(){
                if ($('#inp_files').val()) {
                    if( $('#status').val()!="Select Status"){
                        if( $('#sale_id:checked').val()){
                            $('#orderId').val($('#sale_id:checked').val());
                            $('input:submit').attr('disabled',false);
                            // or, as has been pointed out elsewhere:
                            // $('input:submit').removeAttr('disabled'); 

                        }
                    }
                } 
            }
            );
    });
 
  function fileChange(e) {
 
     document.getElementById('inp_img').value = '';
 
     for (var i = 0; i < e.target.files.length; i++) { 
     
        var file = e.target.files[i];
 
        if (file.type == "image/jpeg" || file.type == "image/png") {
 
           var reader = new FileReader();  
           reader.onload = function(readerEvent) {
 
              var image = new Image();
              image.onload = function(imageEvent) { 
 
                 var max_size = 600;
                 var w = image.width;
                 var h = image.height;
                   
                 if (w > h) {  if (w > max_size) { h*=max_size/w; w=max_size; }
                 } else     {  if (h > max_size) { w*=max_size/h; h=max_size; } }
               
                 var canvas = document.createElement('canvas');
                 canvas.width = w;
                 canvas.height = h;
                 canvas.getContext('2d').drawImage(image, 0, 0, w, h);
                 if (file.type == "image/jpeg") {
                    var dataURL = canvas.toDataURL("image/jpeg", 1.0);
                 } else {
                    var dataURL = canvas.toDataURL("image/png");    
                 }
                 document.getElementById('inp_img').value += dataURL + '|';
              }
              image.src = readerEvent.target.result;
           }
           reader.readAsDataURL(file);
           
            readURL(this);

        } else {
           document.getElementById('inp_files').value = ''; 
           alert('Please only select images in JPG or PNG format.');   
           return false;
        }
     }
 
  }
 
  document.getElementById('inp_files').addEventListener('change', fileChange, false); 
  
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imageHolder').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    </script>
@endsection
